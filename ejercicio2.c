#include <stdio.h>

void imprimir(int *a)
{
    *a = *a + 1 ;
    printf("%d\n",*a);
}

int main()
{
    int a;
    for (int i = 0; i < 5; i++)
    {
        imprimir(&a);
    }
    
    return 0;
}
